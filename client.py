#!/usr/bin/python3
"""Python3 CTF framework developed by: J. McLaurin"""

import socket
import pickle


class Launcher:
    def __init__(self, ipAddr, sock=None):
        self.sock = sock
        # Host can be an address
        self.host = ipAddr  # 'nomadgarage.com'  # socket.gethostname()
        print("Target Server: {0}\nAdjust accordingly".format(
            self.host
            ))
        self.port = 54321

    @property
    def help(self):
        print("Object methods:")
        print("  {0:>30} = {1}".format(
            "(obj).connect",
            "Connect to server"
            ))
        print("  {0:>30} = {1}".format(
            "(obj).getData(#)",
            "Get challenge # data"
            ))
        print("  {0:>30} = {1}".format(
            "(obj).getChallenge(#)",
            "Get challenge #"
            ))
        print("  {0:>30} = {1}".format(
            "(obj).submitAnswer(#, answer)",
            "Submit answer to retrieve key"
            ))
        print("  {0:>30} = {1}".format(
            "(obj).kill",
            "End connection to server"
            ))

    @property
    def connect(self):
        try:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.sock.connect((self.host, self.port))
            response = self.recv_data()
            if response:
                print(response)
        except:
            print("Failed to connect")

    @property
    def kill(self):
        self.sock.shutdown(socket.SHUT_RDWR)
        self.sock.close()
        print("Disconnect sent:")
        try:
            self.sock.recv(1024)
        except:
            print("  Not connected")

    def getData(self, question_number):
        msg = ["getData", question_number]
        self.sock.send(pickle.dumps(msg))
        return self.recv_data()

    def getChallenge(self, question_number):
        msg = ["getChallenge", question_number]
        self.sock.send(pickle.dumps(msg))
        return self.recv_data()

    def submitAnswer(self, question_number, ans):
        msg = ["submitAnswer", question_number, ans]
        self.sock.send(pickle.dumps(msg))
        return self.recv_data()

    def recv_data(self):
        data = self.sock.recv(1024)
        return pickle.loads(data)

print("  Create game object: game = client.Launcher(ipAddress)")
print("  For more guidance: game.help")
