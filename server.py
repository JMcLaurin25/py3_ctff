#!/usr/bin/python3
"""Python3 CTF framework developed by: J. McLaurin"""
import sys
import socket
import threading
import data_bank as bank
import pickle


class Server:
    def __init__(self, ipAddr):
        """
        ------ IMPORTANT ------
        Initializes the problem objects to be used in ctf. Not all of the
        problems need to be here. Only the problems you intend to make
        available to the user.
        """
        self.problems = {
            -1: bank.TEMPLATE_PROBLEM(),
            1: bank.Problem_01(),
            2: bank.Strings_01(),
            3: bank.Strings_02(),
            4: bank.strSlice_1(),
            5: bank.strSlice_2(),
            6: bank.strSlice_3(),
            7: bank.strSlice_4(),
            8: bank.strSlice_5(),
            9: bank.strSlice_6(),
            10: bank.strSlice_7(),
            11: bank.strSlice_8(),
            12: bank.strSlice_9(),
            13: bank.strSlice_10(),
            14: bank.Numbers_01()
            }

        # New Socket object
        self.srv_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        host = ipAddr  # '192.168.1.140'  # socket.gethostname()
        print("  Current server address: {0}\n  Modify accordingly.".format(
            host
            ))
        port = 54321
        self.srv_sock.bind((host, port))

    def get_hashes(self):
        """Prints out the current list of md5 hashes"""
        for challenge in (self.problems.items()):
            print("Problem #{0}: {1}".format(
                challenge[0],
                challenge[1].make_hash(challenge[1].key))
                )

    @property
    def start(self):
        self.srv_sock.listen(50)  # Allow X num of clients
        while True:
            # Establish connection with client.
            client, addr = self.srv_sock.accept()
            if client:
                print("Client {0} connected on port {1}".format(
                    addr[0],
                    addr[1])
                    )
                client.send(pickle.dumps("Connected"))
            client.settimeout(300)  # 5 minute timeout
            threading.Thread(
                target=self.client_listen,
                args=(client, addr)
                ).start()

    def _recv_data(self, client):
        data = client.recv(1024)
        return pickle.loads(data)

    def client_listen(self, client, addr):
        """Listen for clients transmission and parse to corresponding
        methods"""
        while True:
            try:
                """Index[0]: Action,
                Index[1]: problem number,
                Index[2]: answer"""
                cmd_recv = self._recv_data(client)
                print(cmd_recv)
                print(type(cmd_recv))
                if cmd_recv is not []:
                    if cmd_recv[1] in self.problems.keys():
                        cur_problem = self.problems[cmd_recv[1]]

                        print(cmd_recv)
                        if len(cmd_recv) == 3:
                            result = cur_problem.get_action(
                                (cmd_recv[0], cmd_recv[2])
                                )
                        else:
                            result = cur_problem.get_action(
                                (cmd_recv[0], None)
                                )

                        client.send(pickle.dumps(result))
                        # data_recv = self._recv_data(client)

                    else:
                        no_funct = "{0} Method not recognized".format(
                            cmd_recv[0]
                            )
                        client.send(pickle.dumps(no_funct))
                else:
                    raise NameError('Client disconnected')
            except:
                client.close()
                return False


def main():
    argc = len(sys.argv)
    print("Command line args:", str(sys.argv))

    if (argc < 2):
        print("Please provide an IP address: server.py [IP ADDRESS]")
        return

    server = Server(sys.argv[1])

    # Allow server admin to print md5 hashes or start server
    prompt = "CTF Selection:\n  1: to Start Server\n  2: to list MD5 hashes"
    prompt += "\n  3. To exit"
    while True:
        print(prompt)
        selection = input(" >")
        if selection == "1":
            server.start
        elif selection == "2":
            server.get_hashes()
        elif selection == "3":
            break


if __name__ == '__main__':
    main()
