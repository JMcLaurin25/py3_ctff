import hashlib
import random
import string
import time


class BankChallenge:
    """This is a databank that contains a dictionary of the problems/challenges
    starting with problem/challenge 1"""
    def __init__(self, prob_num=None):
        """Add all new problem functions with their key here"""
        self.problem_num = prob_num
        self.seed = time.time()
        self.key = None
        self.fail = "Invalid submission"

    def get_action(self, value):
        """Retrieves the appropriate function based on the received key"""
        if value[0] == "getData":
            return self.getData()
        elif value[0] == "getChallenge":
            return self.getChallenge()
        elif value[0] == "submitAnswer":
            if value[1]:
                print("Value: {0}".format(value[1]))
                return self.submitAnswer(value[1])
        else:
            msg = "Action not known"
            return msg

    def getChallenge(self):
        """This function needs to be overridden"""
        msg = "This method is incomplete"
        return msg

    def getData(self):
        """This function needs to be overridden"""
        msg = "This method is incomplete"
        return msg

    def _solve(self, data):
        """This function needs to be overridden"""
        msg = "This method is incomplete"
        return msg

    def submitAnswer(self, ans):
        print("Data: {0}, Ans: {1}".format(self.getData(), ans))

        if self._solve(self.getData()) == ans:
            return self.make_hash(self.key)
        else:
            return self.fail

    def seed_set(self):
        if (time.time() - self.seed) > 2:
            self.seed = time.time()
        random.seed(self.seed)

    def make_hash(self, keyword):
        new_hash = hashlib.md5()
        new_hash.update(keyword.encode())
        return new_hash.hexdigest()

    def __str__(self):
        return "Problem # {0}".format(self.problem_num)


class TEMPLATE_PROBLEM(BankChallenge):
    """This object is an example of the problems template to create a
    challenge. Use this model to create new challenge problems"""
    # NOTE: Method submitAnswer() is inherited from parent. No need to override
    def __init__(self):
        super().__init__("template")
        self.key = 'Key_found'

    def getChallenge(self):
        """Returns the Challenge definition"""
        problem = "Return the data."
        return problem

    def getData(self):
        """ALWAYS use self.seed_set() to allow proper random value generation.
        Construct random data below"""
        self.seed_set()
        value = random.randint(1, 2000)
        return value

    def _solve(self, data):
        """Solution to challenge"""
        return data


class Problem_01(BankChallenge):
    def __init__(self):
        super().__init__(1)
        self.key = 'problem01'

    def getChallenge(self):
        problem = "Return the data."
        return problem

    def getData(self):
        self.seed_set()
        value = random.randint(1, 2000)
        return value

    def _solve(self, data):
        """Solution to challenge"""
        return data


class Strings_01(BankChallenge):
    """Return the first letter in the dataset."""
    def __init__(self):
        super().__init__('String 01')
        self.key = 'RandIrrelevantStringKey01'

    def getChallenge(self):
        problem = "Return the first letter in the dataset."
        return problem

    def getData(self):
        self.seed_set()
        value = ""
        for i in range(random.randint(4, 10)):
            value += random.choice(string.ascii_letters + string.digits)
        return value

    def _solve(self, data):
        """Solution to challenge"""
        return data[0]


class Strings_02(BankChallenge):
    """Return the only the letters in the dataset."""
    def __init__(self):
        super().__init__('String 02')
        self.key = 'RandIrrelevantStringKey02'

    def getChallenge(self):
        problem = "Return the only the letters in the dataset."
        return problem

    def getData(self):
        self.seed_set()
        value = ""
        for i in range(random.randint(10, 20)):
            value += random.choice(string.ascii_letters + string.digits)
        return value

    def _solve(self, data):
        """Solution to challenge"""
        answer = ""
        for idx in range(len(data)):
            if data[idx].isalpha():
                answer += data[idx]

        return answer


class strSlice_1(BankChallenge):
    def __init__(self):
        super().__init__(21)
        self.key = 'SoSick'

    def getChallenge(self):
        """ Return the last character """
        problem = "Return the last character."
        return problem

    def getData(self):
        """ALWAYS use self.seed_set() to allow proper random value generation.
        Construct random data below"""
        self.seed_set()
        value = ""
        for i in range(random.randint(4, 10)):
            value += random.choice(string.ascii_letters + string.digits)
        return value

    def _solve(self, data):
        """Solution to challenge"""
        return data[-1]


class strSlice_2(BankChallenge):
    def __init__(self):
        super().__init__(22)
        self.key = 'Sickness'

    def getChallenge(self):
        """ Return the 5th character through the 10th letter. """
        problem = "Return the 5th character through the 10th letter, including"
        problem += " the 5th & 10th."
        return problem

    def getData(self):
        """ALWAYS use self.seed_set() to allow proper random value generation.
        Construct random data below"""
        self.seed_set()
        value = ""
        for i in range(random.randint(12, 20)):
            value += random.choice(string.ascii_letters + string.digits)
        return value

    def _solve(self, data):
        """Solution to challenge"""
        return data[4:10]


class strSlice_3(BankChallenge):
    def __init__(self):
        super().__init__(23)
        self.key = 'SickMovesBro'

    def getChallenge(self):
        """ Return the first seven characters. """
        problem = "Return the first seven characters."
        return problem

    def getData(self):
        """ALWAYS use self.seed_set() to allow proper random value generation.
        Construct random data below"""
        self.seed_set()
        value = ""
        for i in range(random.randint(9, 15)):
            value += random.choice(string.ascii_letters + string.digits)
        return value

    def _solve(self, data):
        """Solution to challenge"""
        return data[:6]


class strSlice_4(BankChallenge):
    def __init__(self):
        super().__init__(24)
        self.key = 'ExSickness'

    def getChallenge(self):
        """ Return the last 3 characters. """
        problem = "Return the last three characters."
        return problem

    def getData(self):
        """ALWAYS use self.seed_set() to allow proper random value generation.
        Construct random data below"""
        self.seed_set()
        value = ""
        for i in range(random.randint(6, 10)):
            value += random.choice(string.ascii_letters + string.digits)
        return value

    def _solve(self, data):
        """Solution to challenge"""
        return data[-3:]


class strSlice_5(BankChallenge):
    def __init__(self):
        super().__init__(25)
        self.key = 'SuperSick'

    def getChallenge(self):
        """ Return the characters reversed """
        problem = "Return the string, reversed."
        return problem

    def getData(self):
        """ALWAYS use self.seed_set() to allow proper random value generation.
        Construct random data below"""
        self.seed_set()
        value = ""
        for i in range(random.randint(4, 10)):
            value += random.choice(string.ascii_letters + string.digits)
        return value

    def _solve(self, data):
        """Solution to challenge"""
        return data[::-1]


class strSlice_6(BankChallenge):
    def __init__(self):
        super().__init__(26)
        self.key = 'Sick'

    def getChallenge(self):
        """ Return the first two characters combined with the last two
        characters """
        problem = "Return the first two characters combined with the last two"
        problem += " characters"
        return problem

    def getData(self):
        """ALWAYS use self.seed_set() to allow proper random value generation.
        Construct random data below"""
        self.seed_set()
        value = ""
        for i in range(random.randint(7, 12)):
            value += random.choice(string.ascii_letters + string.digits)
        return value

    def _solve(self, data):
        """Solution to challenge"""
        return data[:2] + data[-2:]


class strSlice_7(BankChallenge):
    def __init__(self):
        super().__init__(27)
        self.key = 'ExtraSick'

    def getChallenge(self):
        """ Return the 6th character. """
        problem = "Return the sixth character"
        return problem

    def getData(self):
        """ALWAYS use self.seed_set() to allow proper random value generation.
        Construct random data below"""
        self.seed_set()
        value = ""
        for i in range(random.randint(8, 24)):
            value += random.choice(string.ascii_letters + string.digits)
        return value

    def _solve(self, data):
        """Solution to challenge"""
        return data[5]


class strSlice_8(BankChallenge):
    def __init__(self):
        super().__init__(28)
        self.key = 'SickSquared'

    def getChallenge(self):
        """ Return the characters at even indexes. """
        problem = "Return the characters at even indexes, pretend zero is"
        problem += " even."
        return problem

    def getData(self):
        """ALWAYS use self.seed_set() to allow proper random value generation.
        Construct random data below"""
        self.seed_set()
        value = ""
        for i in range(random.randint(8, 15)):
            value += random.choice(string.ascii_letters + string.digits)
        return value

    def _solve(self, data):
        """Solution to challenge"""
        return data[::2]


class strSlice_9(BankChallenge):
    def __init__(self):
        super().__init__(29)
        self.key = 'SickBunny'

    def getChallenge(self):
        """ Return the characters at odd indexes starting at index 5. """
        problem = "Return the characters at odd indexes, starting at the 5th"
        problem += " string index."
        return problem

    def getData(self):
        """ALWAYS use self.seed_set() to allow proper random value generation.
        Construct random data below"""
        self.seed_set()
        value = ""
        for i in range(random.randint(12, 19)):
            value += random.choice(string.ascii_letters + string.digits)
        return value

    def _solve(self, data):
        """Solution to challenge"""
        return data[4::2]


class strSlice_10(BankChallenge):
    def __init__(self):
        super().__init__(30)
        self.key = 'ExSickness'

    def getChallenge(self):
        """ Return a string characters at odd indexes followed by even
        indexes. """
        problem = "Return a string with the characters at odd indexes"
        problem += " followed by the characters at even indexes."
        return problem

    def getData(self):
        """ALWAYS use self.seed_set() to allow proper random value generation.
        Construct random data below"""
        self.seed_set()
        value = ""
        for i in range(random.randint(12, 19)):
            value += random.choice(string.ascii_letters + string.digits)
        return value

    def _solve(self, data):
        """Solution to challenge"""
        return data[::2] + data[1::2]


class Numbers_01(BankChallenge):
    """Return the sum of the numbers."""
    def __init__(self):
        super().__init__('Numbers 01')
        self.key = 'RandIrrelevantNumberKey01'

    def getChallenge(self):
        problem = "Return the sum of each number in the dataset."
        problem += " Example: 123 + 456 = 579"
        return problem

    def getData(self):
        self.seed_set()
        value = []
        for num in range(2):
            value.append(random.randint(500, 9999))
        return value

    def _solve(self, data):
        """Solution to challenge"""
        return data[0] + data[1]
